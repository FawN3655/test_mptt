from test_django_work.models import ItemsModel, LanguagesModel

lang = LanguagesModel.objects.filter(name="US").first()
game_genres_list = ["Action games", "Action-adventure games", "Adventure games", "Role-playing games",
                    "Simulation games", "Strategy games", "Sports games", "Puzzle games"]
accessories_list = ["Gamepads", "Headphones"]
Games = "Games"
Consoles = "Consoles"
Accessories = "Accessories"

PLAYSTATION = ItemsModel.objects.create(displayed_text="PLAYSTATION", link="?data=PLAYSTATION", lang=lang)
XBOX = ItemsModel.objects.create(displayed_text="XBOX", link="?data=XBOX", lang=lang)
NINTENDO = ItemsModel.objects.create(displayed_text="NINTENDO", link="?data=NINTENDO", lang=lang)
SEGA = ItemsModel.objects.create(displayed_text="SEGA", link="?data=SEGA", lang=lang)

PS5 = ItemsModel.objects.create(displayed_text="PLAYSTATION 5", parent=PLAYSTATION, link="?data=PS5", lang=lang)
PS5_games = ItemsModel.objects.create(displayed_text=Games, link="?data=ps5_games", lang=lang, parent=PS5)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=ps5_games_{}".format(a), lang=lang, parent=PS5_games)
PS5_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=ps5_consoles", lang=lang, parent=PS5)
PS5_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=ps5_accessories", lang=lang,
                                            parent=PS5)
for a, accessorie in enumerate(accessories_list):
    ItemsModel.objects.create(displayed_text=accessorie, link="?data=PS5_accessories_{}".format(a), lang=lang,
                              parent=PS5_accessories)

PS4 = ItemsModel.objects.create(displayed_text="PLAYSTATION 4", parent=PLAYSTATION, link="?data=PS4", lang=lang)
PS4_games = ItemsModel.objects.create(displayed_text=Games, link="?data=ps4_games", lang=lang, parent=PS4)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=ps4_games_{}".format(a), lang=lang, parent=PS4_games)
PS4_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=ps4_consoles", lang=lang, parent=PS4)
PS4_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=ps4_accessories", lang=lang,
                                            parent=PS4)

PS3 = ItemsModel.objects.create(displayed_text="PLAYSTATION 3", parent=PLAYSTATION, link="?data=PS3", lang=lang)
PS3_games = ItemsModel.objects.create(displayed_text=Games, link="?data=ps3_games", lang=lang, parent=PS3)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=ps3_games_{}".format(a), lang=lang, parent=PS3_games)
PS3_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=ps3_consoles", lang=lang, parent=PS3)
PS3_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=ps3_accessories", lang=lang,
                                            parent=PS3)

PS2 = ItemsModel.objects.create(displayed_text="PLAYSTATION 2", parent=PLAYSTATION, link="?data=PS2", lang=lang)
PS2_games = ItemsModel.objects.create(displayed_text=Games, link="?data=ps2_games", lang=lang, parent=PS2)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=ps2_games_{}".format(a), lang=lang, parent=PS2_games)
PS2_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=ps2_consoles", lang=lang, parent=PS2)
PS2_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=ps2_accessories", lang=lang,
                                            parent=PS2)

PS1 = ItemsModel.objects.create(displayed_text="PLAYSTATION 1", parent=PLAYSTATION, link="?data=PS1", lang=lang)
PS1_games = ItemsModel.objects.create(displayed_text=Games, link="?data=ps1_games", lang=lang, parent=PS1)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=ps1_games_{}".format(a), lang=lang, parent=PS1_games)
PS1_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=ps1_consoles", lang=lang, parent=PS1)
PS1_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=ps1_accessories", lang=lang,
                                            parent=PS1)

PS_VIRA = ItemsModel.objects.create(displayed_text="PS VITA", parent=PLAYSTATION, link="?data=PSV", lang=lang)
PSV_games = ItemsModel.objects.create(displayed_text=Games, link="?data=psV_games", lang=lang, parent=PS_VIRA)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=psV_games_{}".format(a), lang=lang, parent=PSV_games)
PSV_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=psV_consoles", lang=lang, parent=PS_VIRA)
PSV_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=psV_accessories", lang=lang,
                                            parent=PS_VIRA)

PSP = ItemsModel.objects.create(displayed_text="PSP", parent=PLAYSTATION, link="?data=PSP", lang=lang)
PSP_games = ItemsModel.objects.create(displayed_text=Games, link="?data=psP_games", lang=lang, parent=PSP)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=psP_games_{}".format(a), lang=lang, parent=PSP_games)
PSP_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=psP_consoles", lang=lang, parent=PSP)
PSP_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=psP_accessories", lang=lang,
                                            parent=PSP)

XBOX_X_S = ItemsModel.objects.create(displayed_text="XBOX SERIES X|S", parent=XBOX, link="?data=XBOX_X_S", lang=lang)
XBOX_X_S_games = ItemsModel.objects.create(displayed_text=Games, link="?data=XBOX_X_S_games", lang=lang,
                                           parent=XBOX_X_S)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=XBOX_X_S_games_{}".format(a), lang=lang,
                              parent=XBOX_X_S_games)
XBOX_X_S_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=XBOX_X_S_consoles", lang=lang,
                                              parent=XBOX_X_S)
XBOX_X_S_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=XBOX_X_S_accessories",
                                                 lang=lang, parent=XBOX_X_S)
for a, accessorie in enumerate(accessories_list):
    ItemsModel.objects.create(displayed_text=accessorie, link="?data=XBOX_X_S_accessories_{}".format(a), lang=lang,
                              parent=XBOX_X_S_accessories)

XBOX_ONE = ItemsModel.objects.create(displayed_text="XBOX ONE", parent=XBOX, link="?data=XBOX_ONE", lang=lang)
XBOX_ONE_games = ItemsModel.objects.create(displayed_text=Games, link="?data=XBOX_ONE_games", lang=lang,
                                           parent=XBOX_ONE)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=XBOX_ONE_games_{}".format(a), lang=lang,
                              parent=XBOX_ONE_games)
XBOX_ONE_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=XBOX_ONE_consoles", lang=lang,
                                              parent=XBOX_ONE)
XBOX_ONE_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=XBOX_ONE_accessories",
                                                 lang=lang, parent=XBOX_ONE)

XBOX_360 = ItemsModel.objects.create(displayed_text="XBOX 360", parent=XBOX, link="?data=XBOX_360", lang=lang)
XBOX_360_games = ItemsModel.objects.create(displayed_text=Games, link="?data=XBOX_360_games", lang=lang,
                                           parent=XBOX_360)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=XBOX_360_games_{}".format(a), lang=lang,
                              parent=XBOX_360_games)
XBOX_360_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=XBOX_360_consoles", lang=lang,
                                              parent=XBOX_360)
XBOX_360_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=XBOX_360_accessories",
                                                 lang=lang, parent=XBOX_360)

NINTENDO_switch = ItemsModel.objects.create(displayed_text="switch", parent=NINTENDO, link="?data=switch", lang=lang)
NINTENDO_switch_games = ItemsModel.objects.create(displayed_text=Games, link="?data=NINTENDO_switch_games", lang=lang,
                                                  parent=NINTENDO_switch)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=NINTENDO_switch_games_{}".format(a), lang=lang,
                              parent=NINTENDO_switch_games)
NINTENDO_switch_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=NINTENDO_switch_consoles",
                                                     lang=lang, parent=NINTENDO_switch)
NINTENDO_switch_accessories = ItemsModel.objects.create(displayed_text=Accessories,
                                                        link="?data=NINTENDO_switch_accessories", lang=lang,
                                                        parent=NINTENDO_switch)

NINTENDO_3ds_2ds = ItemsModel.objects.create(displayed_text="3DS/2DS", parent=NINTENDO, link="?data=3ds_2ds", lang=lang)
NINTENDO_3ds_2ds_games = ItemsModel.objects.create(displayed_text=Games, link="?data=NINTENDO_3ds_2ds_games", lang=lang,
                                                   parent=NINTENDO_3ds_2ds)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=NINTENDO_3ds_2ds_games_{}".format(a), lang=lang,
                              parent=NINTENDO_3ds_2ds_games)
NINTENDO_3ds_2ds_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=NINTENDO_3ds_2ds_consoles",
                                                      lang=lang, parent=NINTENDO_3ds_2ds)
NINTENDO_3ds_2ds_accessories = ItemsModel.objects.create(displayed_text=Accessories,
                                                         link="?data=NINTENDO_3ds_2ds_accessories", lang=lang,
                                                         parent=NINTENDO_3ds_2ds)

NINTENDO_WI_WIU = ItemsModel.objects.create(displayed_text="WI/WIU", parent=NINTENDO, link="?data=wi_wiu", lang=lang)
NINTENDO_WI_WIU_games = ItemsModel.objects.create(displayed_text=Games, link="?data=NINTENDO_WI_WIU_games", lang=lang,
                                                  parent=NINTENDO_WI_WIU)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=NINTENDO_WI_WIU_games_{}".format(a), lang=lang,
                              parent=NINTENDO_WI_WIU_games)
NINTENDO_WI_WIU_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=NINTENDO_WI_WIU_consoles",
                                                     lang=lang, parent=NINTENDO_WI_WIU_games)
NINTENDO_WI_WIU_accessories = ItemsModel.objects.create(displayed_text=Accessories,
                                                        link="?data=NINTENDO_WI_WIU_accessories", lang=lang,
                                                        parent=NINTENDO_WI_WIU)

NINTENDO_SNES = ItemsModel.objects.create(displayed_text="SNES", parent=NINTENDO, link="?data=SNES", lang=lang)
NINTENDO_SNES_games = ItemsModel.objects.create(displayed_text=Games, link="?data=NINTENDO_SNES_games", lang=lang,
                                                parent=NINTENDO_SNES)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=NINTENDO_SNES_games_{}".format(a), lang=lang,
                              parent=NINTENDO_SNES_games)
NINTENDO_SNES_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=NINTENDO_SNES_consoles",
                                                   lang=lang, parent=NINTENDO_SNES)
NINTENDO_SNES_accessories = ItemsModel.objects.create(displayed_text=Accessories,
                                                      link="?data=NINTENDO_SNES_accessories", lang=lang,
                                                      parent=NINTENDO_SNES)

NINTENDO_NES = ItemsModel.objects.create(displayed_text="NES", parent=NINTENDO, link="?data=NES", lang=lang)
NINTENDO_NES_games = ItemsModel.objects.create(displayed_text=Games, link="?data=NINTENDO_NES_games", lang=lang,
                                               parent=NINTENDO_NES)
NINTENDO_NES_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=NINTENDO_NES_consoles",
                                                  lang=lang, parent=NINTENDO_NES)
NINTENDO_NES_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=NINTENDO_NES_accessories",
                                                     lang=lang, parent=NINTENDO_NES)

NINTENDO_N64 = ItemsModel.objects.create(displayed_text="N64", parent=NINTENDO, link="?data=N64", lang=lang)
NINTENDO_N64_games = ItemsModel.objects.create(displayed_text=Games, link="?data=NINTENDO_N64_games", lang=lang,
                                               parent=NINTENDO_N64)
NINTENDO_N64_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=NINTENDO_N64_consoles",
                                                  lang=lang, parent=NINTENDO_N64)
NINTENDO_N64_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=NINTENDO_N64_accessories",
                                                     lang=lang, parent=NINTENDO_N64)
for a, accessorie in enumerate(accessories_list):
    ItemsModel.objects.create(displayed_text=accessorie, link="?data=NINTENDO_N64_accessories_{}".format(a), lang=lang,
                              parent=NINTENDO_N64_accessories)

SEGA_MEGA_DRIVE = ItemsModel.objects.create(displayed_text="MEGA DRIVE", parent=SEGA, link="?data=MEGA_DRIVE",
                                            lang=lang)
SEGA_MEGA_DRIVE_games = ItemsModel.objects.create(displayed_text=Games, link="?data=SEGA_MEGA_DRIVE_games", lang=lang,
                                                  parent=SEGA_MEGA_DRIVE)
SEGA_MEGA_DRIVE_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=SEGA_MEGA_DRIVE_consoles",
                                                     lang=lang, parent=SEGA_MEGA_DRIVE)
SEGA_MEGA_DRIVE_accessories = ItemsModel.objects.create(displayed_text=Accessories,
                                                        link="?data=SEGA_MEGA_DRIVE_accessories", lang=lang,
                                                        parent=SEGA_MEGA_DRIVE)

SEGA_DREAMCAST = ItemsModel.objects.create(displayed_text="DREAMCAST", parent=SEGA, link="?data=DREAMCAST", lang=lang)
SEGA_DREAMCAST_games = ItemsModel.objects.create(displayed_text=Games, link="?data=SEGA_DREAMCAST_games", lang=lang,
                                                 parent=SEGA_DREAMCAST)
SEGA_DREAMCAST_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=SEGA_DREAMCAST_consoles",
                                                    lang=lang, parent=SEGA_DREAMCAST)
SEGA_DREAMCAST_accessories = ItemsModel.objects.create(displayed_text=Accessories,
                                                       link="?data=SEGA_DREAMCAST_accessories", lang=lang,
                                                       parent=SEGA_DREAMCAST)

lang = LanguagesModel.objects.filter(name="RU").first()
game_genres_list = ["Экшн игры", "Экшн-приключенческие игры", "Приключенческие игры", "Ролевые игры",
                    "Симуляторы", "Стратегические игры", "Спортивные игры", "Логические игры"]
accessories_list = ["Контроллеры", "Наушники"]
Games = "Игры"
Consoles = "Консоли"
Accessories = "Аксессуары"

PLAYSTATION = ItemsModel.objects.create(displayed_text="PLAYSTATION", link="?data=PLAYSTATION", lang=lang)
XBOX = ItemsModel.objects.create(displayed_text="XBOX", link="?data=XBOX", lang=lang)
NINTENDO = ItemsModel.objects.create(displayed_text="NINTENDO", link="?data=NINTENDO", lang=lang)
SEGA = ItemsModel.objects.create(displayed_text="SEGA", link="?data=SEGA", lang=lang)

PS5 = ItemsModel.objects.create(displayed_text="PLAYSTATION 5", parent=PLAYSTATION, link="?data=PS5", lang=lang)
PS5_games = ItemsModel.objects.create(displayed_text=Games, link="?data=ps5_games", lang=lang, parent=PS5)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=ps5_games_{}".format(a), lang=lang, parent=PS5_games)
PS5_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=ps5_consoles", lang=lang, parent=PS5)
PS5_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=ps5_accessories", lang=lang,
                                            parent=PS5)
for a, accessorie in enumerate(accessories_list):
    ItemsModel.objects.create(displayed_text=accessorie, link="?data=PS5_accessories_{}".format(a), lang=lang,
                              parent=PS5_accessories)

PS4 = ItemsModel.objects.create(displayed_text="PLAYSTATION 4", parent=PLAYSTATION, link="?data=PS4", lang=lang)
PS4_games = ItemsModel.objects.create(displayed_text=Games, link="?data=ps4_games", lang=lang, parent=PS4)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=ps4_games_{}".format(a), lang=lang, parent=PS4_games)
PS4_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=ps4_consoles", lang=lang, parent=PS4)
PS4_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=ps4_accessories", lang=lang,
                                            parent=PS4)

PS3 = ItemsModel.objects.create(displayed_text="PLAYSTATION 3", parent=PLAYSTATION, link="?data=PS3", lang=lang)
PS3_games = ItemsModel.objects.create(displayed_text=Games, link="?data=ps3_games", lang=lang, parent=PS3)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=ps3_games_{}".format(a), lang=lang, parent=PS3_games)
PS3_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=ps3_consoles", lang=lang, parent=PS3)
PS3_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=ps3_accessories", lang=lang,
                                            parent=PS3)

PS2 = ItemsModel.objects.create(displayed_text="PLAYSTATION 2", parent=PLAYSTATION, link="?data=PS2", lang=lang)
PS2_games = ItemsModel.objects.create(displayed_text=Games, link="?data=ps2_games", lang=lang, parent=PS2)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=ps2_games_{}".format(a), lang=lang, parent=PS2_games)
PS2_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=ps2_consoles", lang=lang, parent=PS2)
PS2_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=ps2_accessories", lang=lang,
                                            parent=PS2)

PS1 = ItemsModel.objects.create(displayed_text="PLAYSTATION 1", parent=PLAYSTATION, link="?data=PS1", lang=lang)
PS1_games = ItemsModel.objects.create(displayed_text=Games, link="?data=ps1_games", lang=lang, parent=PS1)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=ps1_games_{}".format(a), lang=lang, parent=PS1_games)
PS1_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=ps1_consoles", lang=lang, parent=PS1)
PS1_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=ps1_accessories", lang=lang,
                                            parent=PS1)

PS_VIRA = ItemsModel.objects.create(displayed_text="PS VITA", parent=PLAYSTATION, link="?data=PSV", lang=lang)
PSV_games = ItemsModel.objects.create(displayed_text=Games, link="?data=psV_games", lang=lang, parent=PS_VIRA)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=psV_games_{}".format(a), lang=lang, parent=PSV_games)
PSV_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=psV_consoles", lang=lang, parent=PS_VIRA)
PSV_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=psV_accessories", lang=lang,
                                            parent=PS_VIRA)

PSP = ItemsModel.objects.create(displayed_text="PSP", parent=PLAYSTATION, link="?data=PSP", lang=lang)
PSP_games = ItemsModel.objects.create(displayed_text=Games, link="?data=psP_games", lang=lang, parent=PSP)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=psP_games_{}".format(a), lang=lang, parent=PSP_games)
PSP_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=psP_consoles", lang=lang, parent=PSP)
PSP_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=psP_accessories", lang=lang,
                                            parent=PSP)

XBOX_X_S = ItemsModel.objects.create(displayed_text="XBOX SERIES X|S", parent=XBOX, link="?data=XBOX_X_S", lang=lang)
XBOX_X_S_games = ItemsModel.objects.create(displayed_text=Games, link="?data=XBOX_X_S_games", lang=lang,
                                           parent=XBOX_X_S)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=XBOX_X_S_games_{}".format(a), lang=lang,
                              parent=XBOX_X_S_games)
XBOX_X_S_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=XBOX_X_S_consoles", lang=lang,
                                              parent=XBOX_X_S)
XBOX_X_S_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=XBOX_X_S_accessories",
                                                 lang=lang, parent=XBOX_X_S)
for a, accessorie in enumerate(accessories_list):
    ItemsModel.objects.create(displayed_text=accessorie, link="?data=XBOX_X_S_accessories_{}".format(a), lang=lang,
                              parent=XBOX_X_S_accessories)

XBOX_ONE = ItemsModel.objects.create(displayed_text="XBOX ONE", parent=XBOX, link="?data=XBOX_ONE", lang=lang)
XBOX_ONE_games = ItemsModel.objects.create(displayed_text=Games, link="?data=XBOX_ONE_games", lang=lang,
                                           parent=XBOX_ONE)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=XBOX_ONE_games_{}".format(a), lang=lang,
                              parent=XBOX_ONE_games)
XBOX_ONE_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=XBOX_ONE_consoles", lang=lang,
                                              parent=XBOX_ONE)
XBOX_ONE_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=XBOX_ONE_accessories",
                                                 lang=lang, parent=XBOX_ONE)

XBOX_360 = ItemsModel.objects.create(displayed_text="XBOX 360", parent=XBOX, link="?data=XBOX_360", lang=lang)
XBOX_360_games = ItemsModel.objects.create(displayed_text=Games, link="?data=XBOX_360_games", lang=lang,
                                           parent=XBOX_360)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=XBOX_360_games_{}".format(a), lang=lang,
                              parent=XBOX_360_games)
XBOX_360_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=XBOX_360_consoles", lang=lang,
                                              parent=XBOX_360)
XBOX_360_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=XBOX_360_accessories",
                                                 lang=lang, parent=XBOX_360)

NINTENDO_switch = ItemsModel.objects.create(displayed_text="switch", parent=NINTENDO, link="?data=switch", lang=lang)
NINTENDO_switch_games = ItemsModel.objects.create(displayed_text=Games, link="?data=NINTENDO_switch_games", lang=lang,
                                                  parent=NINTENDO_switch)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=NINTENDO_switch_games_{}".format(a), lang=lang,
                              parent=NINTENDO_switch_games)
NINTENDO_switch_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=NINTENDO_switch_consoles",
                                                     lang=lang, parent=NINTENDO_switch)
NINTENDO_switch_accessories = ItemsModel.objects.create(displayed_text=Accessories,
                                                        link="?data=NINTENDO_switch_accessories", lang=lang,
                                                        parent=NINTENDO_switch)

NINTENDO_3ds_2ds = ItemsModel.objects.create(displayed_text="3DS/2DS", parent=NINTENDO, link="?data=3ds_2ds", lang=lang)
NINTENDO_3ds_2ds_games = ItemsModel.objects.create(displayed_text=Games, link="?data=NINTENDO_3ds_2ds_games", lang=lang,
                                                   parent=NINTENDO_3ds_2ds)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=NINTENDO_3ds_2ds_games_{}".format(a), lang=lang,
                              parent=NINTENDO_3ds_2ds_games)
NINTENDO_3ds_2ds_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=NINTENDO_3ds_2ds_consoles",
                                                      lang=lang, parent=NINTENDO_3ds_2ds)
NINTENDO_3ds_2ds_accessories = ItemsModel.objects.create(displayed_text=Accessories,
                                                         link="?data=NINTENDO_3ds_2ds_accessories", lang=lang,
                                                         parent=NINTENDO_3ds_2ds)

NINTENDO_WI_WIU = ItemsModel.objects.create(displayed_text="WI/WIU", parent=NINTENDO, link="?data=wi_wiu", lang=lang)
NINTENDO_WI_WIU_games = ItemsModel.objects.create(displayed_text=Games, link="?data=NINTENDO_WI_WIU_games", lang=lang,
                                                  parent=NINTENDO_WI_WIU)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=NINTENDO_WI_WIU_games_{}".format(a), lang=lang,
                              parent=NINTENDO_WI_WIU_games)
NINTENDO_WI_WIU_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=NINTENDO_WI_WIU_consoles",
                                                     lang=lang, parent=NINTENDO_WI_WIU_games)
NINTENDO_WI_WIU_accessories = ItemsModel.objects.create(displayed_text=Accessories,
                                                        link="?data=NINTENDO_WI_WIU_accessories", lang=lang,
                                                        parent=NINTENDO_WI_WIU)

NINTENDO_SNES = ItemsModel.objects.create(displayed_text="SNES", parent=NINTENDO, link="?data=SNES", lang=lang)
NINTENDO_SNES_games = ItemsModel.objects.create(displayed_text=Games, link="?data=NINTENDO_SNES_games", lang=lang,
                                                parent=NINTENDO_SNES)
for a, game in enumerate(game_genres_list):
    ItemsModel.objects.create(displayed_text=game, link="?data=NINTENDO_SNES_games_{}".format(a), lang=lang,
                              parent=NINTENDO_SNES_games)
NINTENDO_SNES_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=NINTENDO_SNES_consoles",
                                                   lang=lang, parent=NINTENDO_SNES)
NINTENDO_SNES_accessories = ItemsModel.objects.create(displayed_text=Accessories,
                                                      link="?data=NINTENDO_SNES_accessories", lang=lang,
                                                      parent=NINTENDO_SNES)

NINTENDO_NES = ItemsModel.objects.create(displayed_text="NES", parent=NINTENDO, link="?data=NES", lang=lang)
NINTENDO_NES_games = ItemsModel.objects.create(displayed_text=Games, link="?data=NINTENDO_NES_games", lang=lang,
                                               parent=NINTENDO_NES)
NINTENDO_NES_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=NINTENDO_NES_consoles",
                                                  lang=lang, parent=NINTENDO_NES)
NINTENDO_NES_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=NINTENDO_NES_accessories",
                                                     lang=lang, parent=NINTENDO_NES)

NINTENDO_N64 = ItemsModel.objects.create(displayed_text="N64", parent=NINTENDO, link="?data=N64", lang=lang)
NINTENDO_N64_games = ItemsModel.objects.create(displayed_text=Games, link="?data=NINTENDO_N64_games", lang=lang,
                                               parent=NINTENDO_N64)
NINTENDO_N64_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=NINTENDO_N64_consoles",
                                                  lang=lang, parent=NINTENDO_N64)
NINTENDO_N64_accessories = ItemsModel.objects.create(displayed_text=Accessories, link="?data=NINTENDO_N64_accessories",
                                                     lang=lang, parent=NINTENDO_N64)
for a, accessorie in enumerate(accessories_list):
    ItemsModel.objects.create(displayed_text=accessorie, link="?data=NINTENDO_N64_accessories_{}".format(a), lang=lang,
                              parent=NINTENDO_N64_accessories)

SEGA_MEGA_DRIVE = ItemsModel.objects.create(displayed_text="MEGA DRIVE", parent=SEGA, link="?data=MEGA_DRIVE",
                                            lang=lang)
SEGA_MEGA_DRIVE_games = ItemsModel.objects.create(displayed_text=Games, link="?data=SEGA_MEGA_DRIVE_games", lang=lang,
                                                  parent=SEGA_MEGA_DRIVE)
SEGA_MEGA_DRIVE_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=SEGA_MEGA_DRIVE_consoles",
                                                     lang=lang, parent=SEGA_MEGA_DRIVE)
SEGA_MEGA_DRIVE_accessories = ItemsModel.objects.create(displayed_text=Accessories,
                                                        link="?data=SEGA_MEGA_DRIVE_accessories", lang=lang,
                                                        parent=SEGA_MEGA_DRIVE)

SEGA_DREAMCAST = ItemsModel.objects.create(displayed_text="DREAMCAST", parent=SEGA, link="?data=DREAMCAST", lang=lang)
SEGA_DREAMCAST_games = ItemsModel.objects.create(displayed_text=Games, link="?data=SEGA_DREAMCAST_games", lang=lang,
                                                 parent=SEGA_DREAMCAST)
SEGA_DREAMCAST_consoles = ItemsModel.objects.create(displayed_text=Consoles, link="?data=SEGA_DREAMCAST_consoles",
                                                    lang=lang, parent=SEGA_DREAMCAST)
SEGA_DREAMCAST_accessories = ItemsModel.objects.create(displayed_text=Accessories,
                                                       link="?data=SEGA_DREAMCAST_accessories", lang=lang,
                                                       parent=SEGA_DREAMCAST)
