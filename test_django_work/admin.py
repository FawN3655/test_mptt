from django.contrib import admin
from .models import LanguagesModel, ItemsModel


admin.site.register(ItemsModel)
admin.site.register(LanguagesModel)
