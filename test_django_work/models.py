from django.db import models
from mptt.models import TreeForeignKey, MPTTModel


class LanguagesModel(models.Model):
    name = models.CharField(max_length=5, blank=False, null=False, unique=False)

    def __str__(self):
        return self.name


class ItemsModel(MPTTModel):
    displayed_text = models.CharField(max_length=800, blank=False, null=False, unique=False)
    link = models.CharField(max_length=800, blank=False, null=False, unique=False)
    lang = models.ForeignKey(LanguagesModel, on_delete=models.DO_NOTHING)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    def __str__(self):
        return self.displayed_text

    class MPTTMeta:
        order_insertion_by = ['displayed_text']