from django.template.response import TemplateResponse
from .models import ItemsModel, LanguagesModel
from django.shortcuts import get_object_or_404

texts = {"title": {"RU": "Меню магазина игровых консолей и аксесуаров",
                   "US": "Game consoles and accessories store menu"}}


def index(request):
    """Если нужно чтобы страница содержала статический тест на разных языках"""
    lang = request.GET.get("lang")
    lang = lang if lang else "US"
    lang_model = get_object_or_404(LanguagesModel, name=lang)
    queryset = ItemsModel.objects.filter(lang=lang_model)
    return TemplateResponse(request, '{}/index.html'.format(lang), {"genres": queryset})


def index_2(request):
    """Если весь текст динамический"""
    lang = request.GET.get("lang")
    lang = lang if lang else "US"
    lang_model = get_object_or_404(LanguagesModel, name=lang)
    queryset = ItemsModel.objects.filter(lang=lang_model)
    title = texts.get("title", {}).get(lang, "")
    return TemplateResponse(request, 'index.html', {"genres": queryset, "lang": lang,
                                                                  "title": title})
